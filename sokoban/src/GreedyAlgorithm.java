import java.util.ArrayList;
import java.util.List;

import blink.sokoban.SokobanLevel.SokobanLevelState;
import blink.sokoban.algo.AbstractAlgorithm;
import blink.sokoban.algo.SokobanAction;
import blink.sokoban.algo.SolutionStep;
import blink.util.geom.MapCoords;

public class GreedyAlgorithm extends AbstractAlgorithm
{

	private SokobanLevelState current;

	public GreedyAlgorithm(final SokobanLevelState root)
	{
		this.current = root;
	}

	public int d(final MapCoords b, final MapCoords c)
	{
		int dx = Math.abs(b.x - c.x);
		int dy = Math.abs(b.y - c.y);

		int d = dx + dy;

		if (dx != 0 && dy != 0)
			d += 2;

		return d;
	}

	public int h(final SokobanLevelState state)
	{
		//TODO h != 0

		return 0;
	}

	@Override
	public List<SolutionStep> getSolution()
	{
		ArrayList<SolutionStep> list = new ArrayList<SolutionStep>();

		SokobanAction[] actions = { SokobanAction.GO_NORTH, SokobanAction.GO_EAST, SokobanAction.GO_SOUTH, SokobanAction.GO_WEST };

		boolean stop = false;
		int bestH = Integer.MAX_VALUE;

		while (!stop)
		{
			SokobanLevelState next = null;
			SolutionStep bestStep = null;

			for (SokobanAction a : actions)
			{
				SolutionStep step = new SolutionStep(a);
				SokobanLevelState s = this.current.getSuccessor(step);

				if (s == null)
					continue;

				int h = this.h(s);

				System.out.println(a + " > " + h);

				if (h < bestH)
				{
					bestH = h;
					next = s;
					bestStep = step;

					if (h == 0)
						stop = true;
				}
			}

			if (next == null)
			{
				stop = true;
			}
			else
			{
				list.add(bestStep);
				this.current = next;
			}
		}

		return list;
	}
}