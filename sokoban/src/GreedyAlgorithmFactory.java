import blink.sokoban.SokobanLevel.SokobanLevelState;
import blink.sokoban.algo.AbstractAlgorithm;
import blink.sokoban.algo.AbstractAlgorithmFactory;

public class GreedyAlgorithmFactory extends AbstractAlgorithmFactory
{
	@Override
	public AbstractAlgorithm instantiateAlgorithm(final SokobanLevelState level)
	{
		return new GreedyAlgorithm(level);
	}

	@Override
	public String getAlgorithmName()
	{
		return "Greedy Algorithm, h=0";
	}
}