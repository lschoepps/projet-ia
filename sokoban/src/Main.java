import org.newdawn.slick.SlickException;

import blink.sokoban.SokobanGUI;
import blink.sokoban.algo.SokobanAlgorithms;

public class Main
{
	public static void main(final String args[]) throws SlickException
	{

		SokobanAlgorithms.addAlgorithmFactory(new GreedyAlgorithmFactory());
        SokobanAlgorithms.addAlgorithmFactory(new IdaStarFactory());

		// Execution du programme de Sokoban
		SokobanGUI.main(args);
	}
}
