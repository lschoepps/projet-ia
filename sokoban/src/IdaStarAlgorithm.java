import blink.sokoban.SokobanLevel.SokobanLevelState;
import blink.sokoban.algo.AbstractAlgorithm;
import blink.sokoban.algo.SokobanAction;
import blink.sokoban.algo.SolutionStep;
import blink.util.geom.MapCoords;

import java.util.ArrayList;
import java.util.List;

public class IdaStarAlgorithm extends AbstractAlgorithm {

    private static final int FOUND = -1;
    private SokobanLevelState mCurrent;
    private List<SolutionStep> mSolution;
    private static final SokobanAction[] ACTIONS = {SokobanAction.WAIT, SokobanAction.GO_NORTH, SokobanAction.GO_EAST, SokobanAction.GO_SOUTH, SokobanAction.GO_WEST};
    private ArrayList<SokobanLevelState> mStates;

    public IdaStarAlgorithm(final SokobanLevelState root) {
        mCurrent = root;
    }

    @Override
    public List<SolutionStep> getSolution() {
        mSolution = new ArrayList<>();
        mStates = new ArrayList<>();

        /**
         * Bound is used to limit the estimated scope at each iteration
         */
        int bound = heuristic(mCurrent);
        int target;
        mStates.add(mCurrent);
        while (true) {
            System.out.println("Bound = " + bound);
            target = search(mCurrent, 0, bound);
            if (target == FOUND) return mSolution;
            bound = target;
            if (target == Integer.MAX_VALUE) {
                //Pas de solutions
                return null;
            }
        }
    }

    /**
     * Search recursively the solution to the game with the IDA* algorithm
     *
     * @param state the state of the game
     * @param g     the cost to reach the state
     * @param bound the cost limit
     * @return the minimal cost required to finish the game or FOUND if a solution was found
     */
    private int search(SokobanLevelState state, int g, int bound) {
        int f = g + heuristic(state);
        //System.out.println("f = " + f);
        if (isGoal(state)) return FOUND;
        if (f > bound) return f;
        int min = Integer.MAX_VALUE;
        int actions_ind[] = new int[mCurrent.getPlayerCount()];
        //We avoid the case where every player are waiting
        actions_ind[0] = 1;
        //We make a loop to test every action possible
        while (actions_ind[0] < ACTIONS.length) {
            SokobanAction[] actions = new SokobanAction[mCurrent.getPlayerCount()];
            for (int i = 0; i < mCurrent.getPlayerCount(); i++) {
                actions[i] = ACTIONS[actions_ind[i]];
            }
            SolutionStep step = new SolutionStep(actions);
            SokobanLevelState next = state.getSuccessor(step);
            if (next != null) {
                if (!alreadyReached(next) && !deadlock(next)) {
                    mStates.add(next);
                    mSolution.add(step);
                    int t = search(next, g + 1, bound);
                    if (t == FOUND) return FOUND;
                    //We remove the last step
                    mSolution.remove(mSolution.size() - 1);
                    mStates.remove(next);
                    if (t < min) min = t;
                }
                next.free();
            }
            for (int i = mCurrent.getPlayerCount() - 1; i >= 0; i--) {
                actions_ind[i]++;
                if (i != 0 && actions_ind[i] == ACTIONS.length) {
                    actions_ind[i] = 0;
                } else {
                    break;
                }
            }
        }
        return min;
    }

    private boolean deadlock(SokobanLevelState state) {
        int x, y;
        int cmpt = 0;

        for (MapCoords block : state.getSokobanBlockPositions()) {
            x = block.x;
            y = block.y;
            if (!state.isTarget(x, y)) {
                if ((isWallOrBorder(state, x + 1, y) && (isWallOrBorder(state, x, y + 1) || isWallOrBorder(state, x, y - 1)))
                        || (isWallOrBorder(state, x - 1, y) && (isWallOrBorder(state, x, y + 1) || isWallOrBorder(state, x, y - 1)))) {
                    cmpt++;
                } else if (isHBlocked(state, x, y) && (isBlock(state, x, y + 1) && isHBlocked(state, x, y + 1) || isBlock(state, x, y - 1) && isHBlocked(state, x, y - 1))) {
                    cmpt++;
                } else if (isVBlocked(state, x, y) && (isBlock(state, x + 1, y) && isVBlocked(state, x + 1, y) || isBlock(state, x - 1, y) && isVBlocked(state, x - 1, y))) {
                    cmpt++;
                }
            }
        }

        return cmpt > (state.getSokobanBlockCount() - state.getSokobanTargetCount());
    }

    private boolean isVBlocked(SokobanLevelState state, int x, int y) {
        return isWallOrBorder(state, x, y - 1) || isWallOrBorder(state, x, y + 1);
    }

    private boolean isHBlocked(SokobanLevelState state, int x, int y) {
        return isWallOrBorder(state, x - 1, y) || isWallOrBorder(state, x + 1, y);
    }

    private boolean isBlock(SokobanLevelState state, int x, int y) {
        return state.getSokobanBlockPositions().contains(new MapCoords(x, y));
    }

    private boolean isWallOrBorder(SokobanLevelState state, int x, int y) {
        return !state.inBounds(x, y) || state.isWall(x, y);
    }

    private boolean alreadyReached(SokobanLevelState next) {
        boolean reached = true;
        for (SokobanLevelState state : mStates) {
            for (MapCoords block : next.getSokobanBlockPositions()) {
                if (!state.getSokobanBlockPositions().contains(block)) {
                    reached = false;
                    break;
                }
            }
            if (reached) {
                for (MapCoords player : next.getPlayersPositions()) {
                    if (!state.getPlayersPositions().contains(player)) {
                        reached = false;
                        break;
                    }
                }
            }
            if (reached) {
                return true;
            }
        }

        return false;
    }

    private boolean isGoal(SokobanLevelState state) {
        for (MapCoords target : state.getSokobanTargetPositions()) {
            if (!state.getSokobanBlockPositions().contains(target)) return false;
        }
        return true;
    }

    /**
     * @return the heuristic for the given state
     */
    private int heuristic(SokobanLevelState state) {
        //TODO: Prendre en compte qu'un block ne peut pas aller sur deux cibles à la fois :)
        int h = 0;
        int minAllDistPlayerToBlock = Integer.MAX_VALUE;
        //We calculate, for each target, the minimal distance (with the Manhattan Distance)
        //to move the player next to a block and to move the block to the target.
        //Then because moving to a block can help us to be nearer to an other one,
        //we only add the minimal distance to move a player to a block once for the minimal distance
        for (MapCoords target : state.getSokobanTargetPositions()) {
            //We don't increase the heuristic if a block is already on the target
            if (!state.getSokobanBlockPositions().contains(target)) {
                int minDistBlockToTarget = Integer.MAX_VALUE;
                int minDist = Integer.MAX_VALUE;
                for (MapCoords block : state.getSokobanBlockPositions()) {
                    //We calculate the minimal distance required to be next to the block
                    int minDistPlayerToBlock = Integer.MAX_VALUE;
                    for (MapCoords player : state.getPlayersPositions()) {
                        int dist = manhattanDist(block, player) - 1;
                        //The player need to move the block from the right direction
                        if (player.x > block.x && target.x > block.x || player.x < block.x && target.x < block.x) {
                            dist += 2;
                        }
                        if (player.y > block.y && target.y > block.y || player.y < block.y && target.y < block.y) {
                            dist += 2;
                        }
                        if (minDistPlayerToBlock > dist) {
                            minDistPlayerToBlock = dist;
                        }
                    }
                    //We calculate the minimal distance required to move the block to the target
                    int distBlockToTarget = manhattanDist(target, block);
                    int dist = minDistPlayerToBlock + distBlockToTarget;
                    if (minDist > dist || minDist == dist && minDistBlockToTarget > distBlockToTarget) {
                        minDist = dist;
                        minDistBlockToTarget = distBlockToTarget;
                    }
                }
                //We want the maximal distance required to move the player
                if (minDist - minDistBlockToTarget < minAllDistPlayerToBlock) {
                    minAllDistPlayerToBlock = minDist - minDistBlockToTarget;
                }
                h += minDistBlockToTarget;
            }
        }
        if (minAllDistPlayerToBlock != Integer.MAX_VALUE) {
            h += minAllDistPlayerToBlock;
        }
        return h;
    }

    /**
     * @return the Manhattan distance between 2 coordinates
     */
    private int manhattanDist(MapCoords coords1, MapCoords coords2) {
        int x = coords1.x - coords2.x;
        int y = coords1.y - coords2.y;
        if (x < 0) x = -x;
        if (y < 0) y = -y;
        return x + y;
    }

}