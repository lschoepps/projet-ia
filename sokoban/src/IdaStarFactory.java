import blink.sokoban.SokobanLevel.SokobanLevelState;
import blink.sokoban.algo.AbstractAlgorithm;
import blink.sokoban.algo.AbstractAlgorithmFactory;

public class IdaStarFactory extends AbstractAlgorithmFactory
{
	@Override
	public AbstractAlgorithm instantiateAlgorithm(final SokobanLevelState level)
	{
		return new IdaStarAlgorithm(level);
	}

	@Override
	public String getAlgorithmName()
	{
		return "IDA* Algorithm";
	}
}